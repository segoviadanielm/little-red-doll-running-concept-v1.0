﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    public float lifeTime;

    private void Awake()
    {
        Invoke("DestroyByTime", lifeTime);
    }

    private void Update()
    {
        transform.position += transform.right * speed * Time.deltaTime;
    }

    private void DestroyByTime()
    {
        Destroy(gameObject);
    }
}
