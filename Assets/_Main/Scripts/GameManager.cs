﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using TMPro;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private int maxLifes;

    public static GameManager Instance;
    [SerializeField] private GameObject LMC;
    [SerializeField] private GameObject loseMenu;
    [SerializeField] private GameObject nextLevelMenu;
  
    [SerializeField] private int coins;
    private int counter;


    [SerializeField] private TextMeshProUGUI coinsLabel;
    [SerializeField] private Text coinsLabelWin;
    [SerializeField] private Text lifeLabelWin;
    
    
    [SerializeField] private AudioSource AS;
    private PlayController playController;

    [SerializeField] private int CurrentLifes;

    [SerializeField] private Image[] lifes;

    [SerializeField] private AudioClip winClip;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    private void Start()
    {

            CurrentLifes = maxLifes;
            counter = CurrentLifes - 1;
            coins = 0;
        
        LMC.SetActive(false);
        loseMenu.SetActive(false);
        nextLevelMenu.SetActive(false);

        
            

        coinsLabel.text = $"x{coins}";

        playController = AS.GetComponent<PlayController>();
    }

    public void NextLevel()
    {
        GetLife();
       
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }

    public void LoseLife()
    {

        if(lifes != null)
        {
            CurrentLifes--;
            lifes[counter].gameObject.SetActive(false);
            counter--;
        }

        

        if (CurrentLifes <= 0)
        {
            LoseGame();
        }
    }

    public void WinLevel()
    { 
        LMC.SetActive(true);
        nextLevelMenu.SetActive(true);
        lifeLabelWin.text = $"x{CurrentLifes}";
        coinsLabelWin.text = $"x{coins}";
    }



    public void WinGame()
    {
        AS.Stop();
        AS.clip = winClip;
        AS.Play();
        Debug.Log("Ganaste");
        LMC.SetActive(true);
        nextLevelMenu.SetActive(true);
        //uso esta para ahorrar codigo
        lifeLabelWin.text = $"TOTAL SCORE: {coins * 12}";
    }

    public void LoseGame()
    {
        Debug.Log("Perdiste");
        playController.LoseGame();
        LMC.SetActive(true);
        loseMenu.SetActive(true);
        //Application.Quit();
    }

    public void GetLife()
    {
        if (CurrentLifes < 5)
        {
            CurrentLifes++;
            counter++;
            lifes[counter].gameObject.SetActive(true);
        }
    }

    public int GetCurrentLifes()
    {
        return CurrentLifes;
    }

    public void GetCoins()
    {
        coins += 1;
        coinsLabel.text = $"x{coins}";
    }


}
