﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeAndVboost : MonoBehaviour
{

    private PlayerController player;
    private AudioSource AS;
    [SerializeField] private float Speed;
    [SerializeField] private float MaxSpeed;


    private void Start()
    {
        AS = GetComponent<AudioSource>();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        player = collision.GetComponent<PlayerController>();

        if (player != null)
        {

            if (gameObject.tag == "Life")
            {
                Invoke("DestroyByTime", 0.25f);
                AS.Play();
                player.getLife();
                Debug.Log("GetLife");
            }
            else //es un boost de velocidad
            {
                AS.Play();
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
                
              
                player.Speed += Speed;
                player.maxSpeed += MaxSpeed;
                Invoke("WaitTime", 2f);
               // gameObject.SetActive(false);
            }

        }
    }


    private void DestroyByTime()
    {
        Destroy(gameObject);
    }

    private void WaitTime()
    {
        
        player.Speed -= Speed;
        player.maxSpeed -= MaxSpeed;
        Destroy(gameObject);
    }



}
