﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinController : MonoBehaviour { 

    private AudioSource AS;

    public void Start()
    {
       AS = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController player = collision.GetComponent<PlayerController>();

        AS.Play();

        if (player != null && SceneManager.GetActiveScene().buildIndex <= 2)
        {
            Invoke("WinLevel", 1f);
        }
        else if(player!= null)
        {
            Invoke("WinGame", 1f);
        }

    }

    private void WinLevel()
    {
        GameManager.Instance.WinLevel();
    }

    private void WinGame()
    {
        GameManager.Instance.WinGame();
    }
}
