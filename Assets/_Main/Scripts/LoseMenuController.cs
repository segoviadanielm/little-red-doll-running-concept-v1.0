﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class LoseMenuController : MonoBehaviour
{
    [SerializeField] private Button restart;
    [SerializeField] private Button home;
    [SerializeField] private Button nextLvl;
    [SerializeField] private Button buttonHome;

    [SerializeField] private Button options;
    [SerializeField] private Button optionsHome;
    [SerializeField] private Button optionsSound;
    [SerializeField] private Button optionsBack;

    [SerializeField] private GameObject optionsGameObject;
    [SerializeField] private AudioSource ASGeneral;


    private bool activeSound;
    private bool pause;

    private AudioSource AS;

    private void Start()
    {
        AS = gameObject.GetComponent<AudioSource>();
    }
    private void Awake()
    {
        restart.onClick.AddListener(RestartGame);
        home.onClick.AddListener(GoHome);
        nextLvl.onClick.AddListener(NextLevel);
        buttonHome.onClick.AddListener(GoHome);
        options.onClick.AddListener(Options);
        optionsBack.onClick.AddListener(BackGame);
        optionsSound.onClick.AddListener(DisableSound);
        optionsHome.onClick.AddListener(GoHome);

        activeSound = true;
        pause = false;
    }

    private void Update()
    {
        Time.timeScale = (pause) ? 0 : 1f;
    }
    private void RestartGame()
    {
        AS.Play();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void GoHome()
    {
        AS.Play();
        SceneManager.LoadScene(0);
    }

    private void Options()
    {
        AS.Play();
        optionsGameObject.SetActive(true);
        //Time.timeScale = 1f;
        pause = true;
    }

    private void BackGame()
    {
        AS.Play();
        //Time.timeScale = 0;
        optionsGameObject.SetActive(false);
        pause = false;
  
    }

    private void DisableSound()
    {
        AS.Play();
        activeSound = !activeSound;

        if (activeSound)
            ASGeneral.volume = 0;
        else
            ASGeneral.volume = 0.2f;
    }

    private void NextLevel()
    {
        AS.Play();

        if (SceneManager.GetActiveScene().buildIndex != 3)
            GameManager.Instance.NextLevel();
        else
            SceneManager.LoadScene(0);
    }
}
