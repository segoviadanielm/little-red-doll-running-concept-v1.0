﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    [SerializeField] private Transform groundCheckpoint;
    [SerializeField] private float groundRaycastDistance;
    [SerializeField] private LayerMask groundRaycasLayers;

    private AudioSource AS;
    [SerializeField] private AudioClip onTouchClip;
    [SerializeField] private AudioClip loopClip;

    private MovementController MovementController;

    private void Awake()
    {
        MovementController = GetComponent<MovementController>();
        AS = GetComponent<AudioSource>();
        AS.loop = true;
        AS.clip = loopClip;
        AS.Play();

    }

    private void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(groundCheckpoint.position,
            Vector2.down, groundRaycastDistance, groundRaycasLayers);

        if (hit.collider == null)
        {
            transform.Rotate(new Vector3(0, 180, 0));
        }

        MovementController.Move();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController player = collision.GetComponent<PlayerController>();



        if (player != null)
        {
            AS.Stop();
            AS.loop = false;
            AS.clip = onTouchClip;
            AS.Play();
            player.getTrap();
            Invoke("RestartSound", 0.5f);
        }
    }

    private void RestartSound()
    {
        AS.clip = loopClip;
        AS.loop = true;
        AS.Play();
    }
}

