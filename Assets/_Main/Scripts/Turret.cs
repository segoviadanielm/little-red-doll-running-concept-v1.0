﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public float shootTime;
    public float repeatShootTime;

    private Shooter shooterController;

    private void Awake()
    {
        shooterController = GetComponent<Shooter>();
    }
    
    public void OnDetectPlayer()
    {
        InvokeRepeating("ShootByTime", shootTime, repeatShootTime);
    }

    private void ShootByTime()
    {
        shooterController.Shoot();
    }

    public void CancelShoot()
    {
        CancelInvoke("Shoot");
    }
}
