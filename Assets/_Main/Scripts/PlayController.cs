﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayController : MonoBehaviour
{
    [SerializeField] private GameObject player;
    private MovementController MovementController;
    private AudioSource AS;
    [SerializeField] private AudioClip SoundStart;
    [SerializeField] private AudioClip SoundRun;
    [SerializeField] private AudioClip LoseSound;


    void Start()
    {

        MovementController = player.GetComponent<MovementController>();
        AS = GetComponent<AudioSource>();

        AS.clip = SoundStart;
        AS.Play();
        Invoke("StartGame", SoundStart.length - 0.8f);


    }

    // Update is called once per frame
    private void StartGame()
    {
        AS.clip = SoundRun;
        AS.Play();
        AS.loop = true;
        MovementController.canMove = true;
        
    }

    public void LoseGame()
    {
        AS.Stop();
        AS.loop = false;
        AS.clip = LoseSound;
        AS.Play();
    }
}
