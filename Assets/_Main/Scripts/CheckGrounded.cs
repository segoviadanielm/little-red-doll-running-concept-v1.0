﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGrounded : MonoBehaviour
{
    private PlayerController player;
    void Start()
    {
        player = GetComponentInParent<PlayerController>();
        
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
            player.Grounded = true;

        if (collision.gameObject.tag == "Platform")
        {
            player.transform.parent = collision.transform;
            player.Grounded = true;

        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
            player.Grounded = false;

        if (collision.gameObject.tag == "Platform")
        {
            player.transform.parent = null;
            player.Grounded = false;
         
        }
    }
}

