﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform spawnBulletPosition;
    public AudioClip shootAudio;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Shoot()
    {
        audioSource.Stop();
        audioSource.clip = shootAudio;
        audioSource.Play();
        GameObject bullet = Instantiate(bulletPrefab);
        bullet.transform.position = spawnBulletPosition.position;
        bullet.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles);
    }
}
