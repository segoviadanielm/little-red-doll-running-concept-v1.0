﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D RB;
    private MovementController MC;
    private Animator animator;
    private AudioSource AS;
    private Vector3 positionBeforeJump;

    public bool Alive;
    public float Speed;
    public float maxSpeed;
    public bool Grounded;
    public bool Skill;
    public float JumpForce;
    public int Coins;
    public bool CanGetTrap;
    [SerializeField] private int maxLifes;
    public bool isPlatform;

    private bool FirstJump;
    private bool DoubleJump;

    public UnityEvent OnDead = new UnityEvent();





    void Start()
    {

        MC = GetComponent<MovementController>();
        animator = GetComponent<Animator>();
        RB = GetComponent<Rigidbody2D>();
        AS = GetComponent<AudioSource>();
        positionBeforeJump = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);

        Alive = true;
        Skill = false;
        Coins = 0;
        CanGetTrap = true;



    }
    // Update is called once per frame
    void Update()
    {
        animator.SetBool("Grounded", Grounded);
        animator.SetFloat("Velocity", Mathf.Abs(RB.velocity.x));

        if (Alive && MC.canMove)
        {

            // animator.SetBool("IsRunning", true);

            //  MC.Move();
            animator.SetBool("Grounded", Grounded);

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (Grounded)
                {
                    if (!isPlatform)
                    {
                        positionBeforeJump.x = gameObject.transform.position.x;
                        positionBeforeJump.y = gameObject.transform.position.y;
                    }

                    FirstJump = true;
                    DoubleJump = true;
                    AS.Play();
                }
                else if (DoubleJump)
                {
                    FirstJump = true;
                    DoubleJump = false;
                    AS.Play();
                }

                animator.SetBool("UpDown", true);

            }

            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                animator.SetBool("UpDown", false);
            }

        }

    }

    private void FixedUpdate()
    {
        Run();

        if (FirstJump)
        {
            Jump();
            FirstJump = false;
        }
    }



    private void Run()
    {
        if (MC.canMove)
        {

            Vector3 fixedVelocity = RB.velocity;
            fixedVelocity.x *= 0.93f;

            if (Grounded)
            {
                RB.velocity = fixedVelocity;
            }

            float h = Input.GetAxis("Horizontal");



            RB.AddForce(Vector2.right * Speed * h);

            float limitedSpeed = Mathf.Clamp(RB.velocity.x, -maxSpeed, maxSpeed);
            RB.velocity = new Vector2(limitedSpeed, RB.velocity.y);

            if (h >= 0.0f)
                transform.localScale = new Vector3(1f, 1f, 1f);
            if (h < 0.0f)
                transform.localScale = new Vector3(-1f, 1f, 1f);

        }


    }
    private void Jump()
    {
        if (MC.canMove)
        {

            if (FirstJump)
            {
                RB.velocity = new Vector2(RB.velocity.x, 0);
                RB.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);

            }


        }
    }

    public void getCoin()
    {

        GameManager.Instance.GetCoins();
        Coins += 1;
    }


    public void getTrap()
    {
        Invoke("Respawn", 0.5f);
        Invoke("GetTrapEnabled", 2f);
        gameObject.SetActive(false);
    }

    public void getLife()
    {
        GameManager.Instance.GetLife();
    }

    private void Respawn()
    {
        if (CanGetTrap)
        {
            CanGetTrap = false;
            GameManager.Instance.LoseLife();
        }

        gameObject.transform.position = positionBeforeJump;
        gameObject.SetActive(true);
    }
    
    private void GetTrapEnabled()
    {
        CanGetTrap = true;
    }

}
