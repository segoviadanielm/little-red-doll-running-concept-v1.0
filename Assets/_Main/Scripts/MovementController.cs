﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [SerializeField] private float Speed;
    [SerializeField] public bool canMove;

    public void Start()
    {
        canMove = false;
    }

    public void Move()
    {
       transform.position += Speed * transform.right * Time.deltaTime;
    }


}
