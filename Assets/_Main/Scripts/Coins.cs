﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{

    private AudioSource AS;


    private void Start()
    {
        AS = GetComponent<AudioSource>();
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController player = collision.GetComponent<PlayerController>();

        if (player != null)
        {
            Invoke("DestroyByTime", 0.25f);
            AS.Play();
            player.getCoin();
            Debug.Log("Pick Coin");
        }
    }


    private void DestroyByTime()
    {
        Destroy(gameObject);
    }

}
