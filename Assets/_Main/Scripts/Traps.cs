﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Traps : MonoBehaviour
{

    private AudioSource AS;

    public void Start()
    {
        AS = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController player = collision.GetComponent<PlayerController>();

        if(player != null)
        {
            // player.getTrap();
            if (player.CanGetTrap || gameObject.tag == "Water")
            {
                player.gameObject.SetActive(false);
                AS.Play();
            }
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController player = collision.GetComponent<PlayerController>();

        if(player != null && player.CanGetTrap)
        {
            player.getTrap();
        }
    }

}
