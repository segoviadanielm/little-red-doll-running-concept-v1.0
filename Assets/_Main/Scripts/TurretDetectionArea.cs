﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretDetectionArea : MonoBehaviour
{
    private Turret turret;

    private void Awake()
    {
        turret = GetComponentInParent<Turret>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController player = collision.GetComponent<PlayerController>();

        if (player != null)
        {
            turret.OnDetectPlayer();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController player = collision.GetComponent<PlayerController>();

        if (player != null)
        {
            turret.CancelInvoke();
        }
    }
}
